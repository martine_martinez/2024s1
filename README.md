# Sistemas Operativos

## 2024 Semestre 1

En este respositorio se irá publicando material de la materia (slides, código, tutoriales, etc).

### Horarios y Aulas

- Lunes 19:00 Hs - Aula 52  ~~Aula 213~~
- Miercoles 19:00 Hs - Aula 37B y 38B



## Contacto

- tpi-est-so@listas.unq.edu.ar
        Dudas, consultas, avisos para comunicación con todos los estudiantes y docentes.

- tpi-doc-so@listas.unq.edu.ar
 Cualquier duda de la materia que no es para todos los compañeros, envíenla a esta lista

-  Server de Discord: https://discord.com/invite/fnKNT7zX

-  Responder la encuesta: https://docs.google.com/forms/d/e/1FAIpQLSdTtYJMYsujRGVAf6XgiWrTaV4bdWDX7WKjluFNdfqzX6lVUA/viewform?usp=sf_link


## Programa de la cursada

-  [Descripción de la cursada](./slides/00_curso.pdf)
-  [Temas / Libros](./libros/)


## Slides de las Clases
<!--
- [1 - Intro](./slides/01_intro.pdf)
- [2 - Procesos](./slides/02_procesos.pdf)
- [2.1 - Threads](./slides/02-1_threads.pdf)
- [3 - CPU Scheduling](./slides/03_scheduling.pdf)
- [4 - Memoria - Asignación Continua](./slides/04_memoria.pdf)
- [5 - Memoria - Paginación](./slides/05_paginacion.pdf)
- [6 - Memoria - Paginación Bajo Demanda](./slides/06_virtualMemory.pdf)

-->

## Trabajos Prácticos

[Herramientas TPs](./practicas/README.md)



- [práctica 1](./practicas/practica_1)  -  fecha de entrega: 27/03/24
- [práctica 2](./practicas/practica_2)  -  fecha de entrega: 03/04/24
<!---
- [práctica 3](./practicas/practica_3)  -  fecha de entrega: TBD
- [práctica 4](./practicas/practica_4)  -  fecha de entrega: TBD
- [práctica 5](./practicas/practica_5)  -  fecha de entrega: TBD
- [práctica 6](./practicas/practica_6)  -  fecha de entrega: TBD


## Ejecrcicios

- [Gantt Scheduler](./practicas/ejercicios/1_scheduler)
- [Asignacion Continua](./practicas/ejercicios/2_asignacion_continua)
- [Paginacion](./practicas/ejercicios/3_paginacion_mmu)
- [Paginacion Bajo Demanda - Page Fault](./practicas/ejercicios/4_virtual_mem_pagefault)
- [Paginacion Bajo Demanda - Selección de Víctima](./practicas/ejercicios/5_virtual_mem_victima)




## Parcial
- [1ra Fecha - TBD](./parcial.md)


-->



